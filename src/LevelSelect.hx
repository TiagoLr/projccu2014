package ;

import haxe.ui.toolkit.core.XMLController;
import haxe.ui.toolkit.events.UIEvent;
import openfl.events.MouseEvent;

@:build(haxe.ui.toolkit.core.Macros.buildController("assets/ui/level_select.xml"))
class LevelSelect extends Screen {
	public function new() {
	
		btnPlayLevel1.onClick = function(e:UIEvent) { startLevel(1); }
		btnPlayLevel2.onClick = function(e:UIEvent) { startLevel(2); }
		btnPlayLevel3.onClick = function(e:UIEvent) { startLevel(3); }
		
		level1.sprite.mouseChildren = false;
		level2.sprite.mouseChildren = false;
		level3.sprite.mouseChildren = false;
		
		
		level1.onClick = function(e:UIEvent) {
			btnPlayLevel1.alpha = 1;
			btnPlayLevel2.alpha = 0;
			btnPlayLevel3.alpha = 0;
		}
		
		level2.onClick = function(e:UIEvent) {
			btnPlayLevel2.alpha = 1;
			btnPlayLevel1.alpha = 0;
			btnPlayLevel3.alpha = 0;
		}
		
		level3.onClick = function(e:UIEvent) {
			btnPlayLevel3.alpha = 1;
			btnPlayLevel2.alpha = 0;
			btnPlayLevel1.alpha = 0;
		}
	}
	
	override function onVoltar(_) {
		Main.switchScreen(new MainMenu());
	}
	
	function startLevel(level:Int) {
		switch(level) {	
			case 1:
				Main.switchScreen(new LetterSoup());
			case 2:
				Main.switchScreen(new Quiz());
			case 3:
				Main.switchScreen(new Crosswords());
		}
	}
}