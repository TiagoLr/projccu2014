package ;
import haxe.ui.toolkit.core.XMLController;
import haxe.ui.toolkit.events.UIEvent;
import openfl.events.Event;
import openfl.events.MouseEvent;

/**
 * ...
 * @author TiagoLr
 */
class Screen extends XMLController {
	
	public function new(path:String) { 
		super(path);  
		this.view.addEventListener(Event.ADDED_TO_STAGE, onAdded);
	}
	
	private function onAdded(e:Event):Void {
		this.view.removeEventListener(Event.ADDED_TO_STAGE, onAdded);
		
		var btnMenu = Reflect.getProperty(this, "btnMenu");
		if (btnMenu != null) {
			btnMenu.addEventListener(MouseEvent.CLICK, function() {
				Main.switchScreen(new MainMenu());
			});
		}
		
		var btnHelp = Reflect.getProperty(this, "btnHelp");
		if (btnHelp != null) {
			btnHelp.addEventListener(MouseEvent.CLICK, function() {
				showHelp();
			});
		}
		
		var btnVoltar = Reflect.getProperty(this, "btnVoltar");
		if (btnVoltar != null) {
			btnVoltar.addEventListener(MouseEvent.CLICK, onVoltar);
		}
		
	}
	
	function onVoltar(_) {}
	
	public function destroy() { }
	
	function showHelp() {
		new CommonPopup("Nao foi adicionada ajuda a este ecrã", "", "Continuar");
	}
}