package ;
import haxe.ui.toolkit.controls.popups.Popup;
import haxe.ui.toolkit.core.PopupManager;
import haxe.ui.toolkit.core.XMLController;
import haxe.ui.toolkit.events.UIEvent;

/**
 * ...
 * @author TiagoLr
 */
@:build(haxe.ui.toolkit.core.Macros.buildController("assets/ui/common/popup.xml"))
class CommonPopup extends XMLController {

	var thisPopup:Popup;
	
	public function new(popupText:String="", btn1Txt:String = "", btn2Txt:String = "", onPressOne:Void->Void = null, onPressTwo:Void->Void = null, moreInfo:String = "") {
		
		this.popupText.text = popupText;
		
		if (btn1Txt == "" || btn1Txt == null) {
			popupBtn1.visible = false;
		} else {
			popupBtn1.text = btn1Txt;
			popupBtn1.onClick = function(e:UIEvent) {
				if (onPressOne != null) {
					onPressOne();
				}
				PopupManager.instance.hidePopup(thisPopup);	
			}
			
		}
		
		if (btn2Txt == "" || btn2Txt == null) {
			popupBtn2.visible = false;
		} else {
			popupBtn2.text = btn2Txt;
			
			if (moreInfo != "") {
				popupBtn2.onClick = function(_) {
					this.popupText.text = moreInfo;
				}
			} else {
				popupBtn2.onClick = function(e:UIEvent) {
					if (onPressTwo != null) {
						onPressTwo();
					}
					PopupManager.instance.hidePopup(thisPopup);
				}
			}
		}
		
		
		thisPopup = PopupManager.instance.showCustom(this.view, null, {useDefaultTitle:false, modal:true});
	}
	
}