package ;

import haxe.Timer;
import haxe.ui.toolkit.core.XMLController;
import haxe.ui.toolkit.events.UIEvent;
import openfl.display.Sprite;
import openfl.events.Event;
import openfl.events.MouseEvent;
import openfl.display.BlendMode;
import openfl.geom.Point;

@:build(haxe.ui.toolkit.core.Macros.buildController("assets/ui/letter_soup.xml"))
class LetterSoup extends Screen {
	static public inline var START_X:Int = 72;
	static public inline var START_Y:Int = 260;
	static public inline var END_X:Int = 292;
	static public inline var END_Y:Int = 260;
	static public inline var MARGIN:Int = 10;
	
	var overlay:Sprite;
	var start:Point;
	var end:Point;
	
	public function new() {
		overlay = new Sprite();
		overlay.blendMode = BlendMode.MULTIPLY;
		sopaLetras.sprite.addChild(overlay);
		overlay.addEventListener(MouseEvent.MOUSE_DOWN, onDownOverlay);
		overlay.addEventListener(MouseEvent.MOUSE_UP, onUpOverlay);
		clearOverlay();
	}
	
	function clearOverlay() {
		overlay.graphics.clear();
		overlay.graphics.beginFill(0, 0.0);
		overlay.graphics.drawRect(0, 0, sopaLetras.sprite.width, sopaLetras.sprite.height);
	}
	
	override public function destroy() {
		super.destroy();
		
		// remove event listeners if they exist
		overlay.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
		overlay.removeEventListener(MouseEvent.MOUSE_DOWN, onDownOverlay);
		overlay.removeEventListener(MouseEvent.MOUSE_UP, onUpOverlay);
	}
	
	private function onDownOverlay(e:MouseEvent):Void {
		start = new Point(overlay.mouseX, overlay.mouseY);
		overlay.addEventListener(Event.ENTER_FRAME, onEnterFrame);
	}
	
	private function onUpOverlay(e:MouseEvent):Void {
		overlay.removeEventListener(Event.ENTER_FRAME, onEnterFrame);
		end = new Point(overlay.mouseX, overlay.mouseY);
		getResult(true);
	}
	
	private function onEnterFrame(e:Event):Void {
		end = new Point(overlay.mouseX, overlay.mouseY);
		clearOverlay();
		getResult(false);
	}
	
	function getResult(final:Bool) {
		var wordFound:Bool = false;
		
		if (!final) {
			overlay.graphics.lineStyle(26, 0xFF8000); // use orange color
		} else if (Math.abs(start.x - START_X) < MARGIN
			&& Math.abs(start.y - START_Y) < MARGIN
			&& Math.abs(end.x - END_X) < MARGIN 
			&& Math.abs(end.y - END_Y) < MARGIN)
		{
			overlay.graphics.lineStyle(26, 0x00FF00); // use green color
			wordFound = true;
		} else {
			overlay.graphics.lineStyle(26, 0xFF0000); // use red color
		}
		
		overlay.graphics.moveTo(start.x, start.y);
		overlay.graphics.lineTo(end.x, end.y);
		
		if (wordFound) {
			overlay.removeEventListener(MouseEvent.MOUSE_DOWN, onDownOverlay);
			overlay.removeEventListener(MouseEvent.MOUSE_UP, onUpOverlay);
			Timer.delay(function () { startAnimation(); } , 500 );
		}
	}
	
	public function startAnimation() {
		overlay.visible = false;
		Timer.delay( function () {
			overlay.visible = true;
			Timer.delay( function () {
				overlay.visible = false;
				Timer.delay( function () {
					overlay.visible = true;
					Timer.delay( function () {
						overlay.visible = false;
						Timer.delay( function () {
							overlay.visible = true;
							Timer.delay(finishAnimation, 500);
						}, 500);
					}, 500);
				}, 500);
			}, 500);
		}, 500);
	}
	
	public function finishAnimation() {
		new CommonPopup("Uma firewall (em português: Parede de fogo) é um dispositivo de uma rede de computadores que tem por objectivo aplicar uma política de segurança a um determinado ponto da rede.",
						 "Continuar", "Pesquisar mais informações", 
						 function() { Main.switchScreen(new LevelSelect()); },
						 function() { Main.switchScreen(new LevelSelect()); }, "Não existem mais informações." ); 
	}
	
	override function onVoltar(_) {
		Main.switchScreen(new LevelSelect());
	}

	
	

}