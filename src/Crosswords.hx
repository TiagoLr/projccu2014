package ;

import haxe.Timer;
import haxe.ui.toolkit.core.XMLController;
import haxe.ui.toolkit.events.UIEvent;
import openfl._v2.geom.Point;
import openfl.events.Event;
import openfl.events.FocusEvent;
import openfl.events.KeyboardEvent;
import openfl.filters.GlowFilter;
import openfl.text.TextFormatAlign;
import openfl.display.Bitmap;
import openfl.Assets;
import openfl.display.Sprite;
import openfl.display.BlendMode;
import openfl.display.Stage;
import openfl.events.MouseEvent;
import openfl.Lib;
import openfl.text.TextField;

@:build(haxe.ui.toolkit.core.Macros.buildController("assets/ui/crosswords.xml"))
class Crosswords extends Screen {
	var overlay:Sprite;
	
	var wordAntiV:Sprite;
	var wordHacker:Sprite;
	var wordCloud:Sprite;
	
	var selectedWord:Sprite;
	var inputTxt:TextField;
	
	var outlineOverlay:Sprite;
	
	var txtPopup:String;
	var txtAntiV = "Os antivírus são programas de computador concebidos para prevenir, detectar e eliminar vírus de computador.";
	var txtCloud = "O conceito de computação em nuvem (em inglês, cloud computing) refere-se à utilização da memória e das capacidades de armazenamento e cálculo de computadores e servidores compartilhados e interligados por meio da Internet.";
	var txtHacker = "Em informática, hacker é um indivíduo que se dedica, com intensidade incomum, a conhecer e modificar os aspectos mais internos de dispositivos, programas e redes de computadores.";
	
	public function new() {
		wordAntiV = new Sprite(); wordAntiV.alpha = 0.01;
		wordHacker = new Sprite(); wordHacker.alpha = 0.01;
		wordCloud = new Sprite(); wordCloud.alpha = 0.01;
		
		wordAntiV.blendMode = BlendMode.MULTIPLY; 
		wordAntiV.x = 131; 
		wordAntiV.y = 7;
		
		wordHacker.blendMode = BlendMode.MULTIPLY;
		wordHacker.x = 23;
		wordHacker.y = 175;
		
		wordCloud.blendMode = BlendMode.MULTIPLY;
		wordCloud.x = 67;
		wordCloud.y = 177;
		
		wordAntiV.addChild(new Bitmap(Assets.getBitmapData("img/tabuleiroAntivirus.png")));
		wordHacker.addChild(new Bitmap(Assets.getBitmapData("img/tabuleiroHacker.png")));
		wordCloud.addChild(new Bitmap(Assets.getBitmapData("img/tabuleiroCloud.png")));
		
		sopaLetras.sprite.addChild(wordAntiV);
		sopaLetras.sprite.addChild(wordHacker);
		sopaLetras.sprite.addChild(wordCloud);
		
		outlineOverlay = new Sprite();
		sopaLetras.sprite.addChild(outlineOverlay);
		
		wordAntiV.addEventListener(MouseEvent.CLICK, onClickWord );
		wordHacker.addEventListener(MouseEvent.CLICK, onClickWord );
		wordCloud.addEventListener(MouseEvent.CLICK, onClickWord );
		
		var td = Reflect.getProperty(wordInput, "_textDisplay");
		inputTxt = Reflect.getProperty(td, "_tf");
		var format = inputTxt.getTextFormat();
		format.align = TextFormatAlign.CENTER;
		inputTxt.setTextFormat(format);
		
		//inputTxt.addEventListener(FocusEvent.FOCUS_IN, function (_) { trace("FOCCCUSSS"); } );
		//Lib.current.stage.focus = inputTxt;
		
		wordInput.style.backgroundColor = 0x00BBFF;
		wordInput.invalidate();
		wordInput.visible = false;
	}
	
	override public function destroy() {
		inputTxt.removeEventListener(KeyboardEvent.KEY_DOWN, onEnterPressed);
		wordAntiV.removeEventListener(MouseEvent.CLICK, onClickWord );
		wordHacker.removeEventListener(MouseEvent.CLICK, onClickWord );
		wordCloud.removeEventListener(MouseEvent.CLICK, onClickWord );
		super.destroy();
	}
	
	private function onClickWord(e:MouseEvent):Void {
		
		selectedWord = e.currentTarget;
		
		outlineOverlay.graphics.clear();
		var glow = new GlowFilter(0x0080FF);
		outlineOverlay.filters = [glow];
		outlineOverlay.graphics.beginFill(0xFF0000, 0);
		outlineOverlay.graphics.lineStyle(2, 0x0080FF);
		
		inputTxt.text = "";
		
		if (selectedWord == wordAntiV) {
			outlineOverlay.graphics.drawRect(132, 8, 23, 242);
			inputTxt.maxChars = 9;
		} else if (selectedWord == wordCloud) {
			outlineOverlay.graphics.drawRect(66, 176, 24, 123);
			inputTxt.maxChars = 5;
		} else if (selectedWord == wordHacker) {
			outlineOverlay.graphics.drawRect(23, 176, 132, 25);
			inputTxt.maxChars = 6;
		}
		
		wordInput.visible = true;
		inputTxt.removeEventListener(KeyboardEvent.KEY_DOWN, onEnterPressed); // prevent event listener duplicates
		inputTxt.addEventListener(KeyboardEvent.KEY_DOWN, onEnterPressed);
		Lib.current.stage.focus = inputTxt;
	}
	
	private function onEnterPressed(e:KeyboardEvent):Void {
		if (e.keyCode == 13) {
			var txt = inputTxt.text.toLowerCase();
			
			if (selectedWord == wordAntiV && txt == 'antivírus' || txt == 'antivirus' || txt == 'antivìrus') {
				wordAntiV.alpha = 1;
				wordAntiV.removeEventListener(MouseEvent.CLICK, onClickWord );
				unselectWord();
				txtPopup = txtAntiV;
				setInputTempColor(0x00FF00);
				Timer.delay(showCorrectPopup, 1000);
				
			} else if (selectedWord == wordHacker && txt == 'hacker') {
				wordHacker.alpha = 1;
				wordHacker.removeEventListener(MouseEvent.CLICK, onClickWord );
				unselectWord();
				txtPopup = txtHacker;
				setInputTempColor(0x00FF00);
				Timer.delay(showCorrectPopup, 1000);
				
			} else if (selectedWord == wordCloud && txt == 'cloud') {
				wordCloud.alpha = 1;
				wordCloud.removeEventListener(MouseEvent.CLICK, onClickWord );
				unselectWord();
				txtPopup = txtCloud;
				setInputTempColor(0x00FF00);
				Timer.delay(showCorrectPopup, 1000);
			} else {
				setInputTempColor(0xFF0000);
			}
		} 
	}
	
	function setInputTempColor(color:Int) {
		wordInput.style.backgroundColor = color;
		Timer.delay(function() { wordInput.style.backgroundColor = 0x00BBFF; }, 1000);
	}
	
	function unselectWord() {
		outlineOverlay.graphics.clear();
		wordInput.visible = false;
		selectedWord = null;
	}
	
	function showCorrectPopup() {
		txtPopup = "Certo!!\n\n" + txtPopup;
		var f1 = null;
		var f2 = null;
		
		if (wordAntiV.alpha == 1 && wordCloud.alpha == 1 && wordAntiV.alpha == 1) {
			f1 = function() { Main.switchScreen(new LevelSelect()); }
			f2 = function() { Main.switchScreen(new LevelSelect()); }
		}
		
		new CommonPopup(txtPopup, "Continuar", "Pesquisar mais informações", f1, f2, "Não existem mais informações."); 
	}
	
	override function onVoltar(_) {
		Main.switchScreen(new LevelSelect());
	}
	
}