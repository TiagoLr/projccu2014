package ;

import haxe.ui.toolkit.containers.VBox;
import haxe.ui.toolkit.core.Root;
import haxe.ui.toolkit.core.Toolkit;
import haxe.ui.toolkit.themes.GradientTheme;

class Main {
	
	static var _root:Root;
	static var _currentScreen:Screen;
	static var _box:VBox;
	
	public static function main() {
		Toolkit.theme = new GradientTheme();
		Toolkit.init();
		Toolkit.addStyleSheet("css/main.css");
		_root = Toolkit.open();
		
		_box = new VBox();
		_box.percentWidth = 100;
		_box.percentHeight = 100;
		_root.addChild(_box);
		
		switchScreen(new MainMenu());
	}
	
	public static function switchScreen(screen:Screen) {
		if (_currentScreen != null) {
			_currentScreen.destroy();
			_box.removeChild(_currentScreen.view);
		}		
		
		_currentScreen = screen;
		_box.addChild(_currentScreen.view);
	}
}
