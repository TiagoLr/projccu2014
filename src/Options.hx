package ;

import haxe.ui.toolkit.core.XMLController;
import haxe.ui.toolkit.events.UIEvent;

@:build(haxe.ui.toolkit.core.Macros.buildController("assets/ui/options.xml"))
class Options extends Screen {
	public function new() {
	}
	
	override function onVoltar(_) {
		Main.switchScreen(new MainMenu());
	}
}