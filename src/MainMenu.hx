package ;

import haxe.ui.toolkit.core.XMLController;
import haxe.ui.toolkit.events.UIEvent;

@:build(haxe.ui.toolkit.core.Macros.buildController("assets/ui/main_menu.xml"))
class MainMenu extends Screen {
	public function new() {
		btnMenu.visible = false;
		btnVoltar.visible = false;
		
		btnPlay.onClick = function(e:UIEvent) {
			Main.switchScreen(new LevelSelect());
		}
		
		btnResults.onClick = function(e:UIEvent) {
			Main.switchScreen(new Results());
		}
		
		btnOptions.onClick = function(e:UIEvent) {
			Main.switchScreen(new Options());
		}
		
	}
}