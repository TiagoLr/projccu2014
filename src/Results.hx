package ;

import haxe.ui.toolkit.core.XMLController;
import haxe.ui.toolkit.events.UIEvent;

@:build(haxe.ui.toolkit.core.Macros.buildController("assets/ui/results.xml"))
class Results extends Screen {
	public function new() {
		btnResult1.onClick = function (e:UIEvent) {
			new CommonPopup("Para desbloquear esta conquista precisa completar o nível 1.", "Continuar");
		}
		
		btnResult2.onClick = function (e:UIEvent) {
			new CommonPopup("Para desbloquear esta conquista precisa completar o nível 2.", "Continuar");
		}
		
		btnResult3.onClick = function (e:UIEvent) {
			new CommonPopup("Para desbloquear esta conquista precisa completar o nível 3.", "Continuar");
		}
	}
	
	override function onVoltar(_) {
		Main.switchScreen(new MainMenu());
	}
}