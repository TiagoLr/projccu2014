package ;

import haxe.Timer;
import haxe.ui.toolkit.controls.Button;
import haxe.ui.toolkit.core.XMLController;
import haxe.ui.toolkit.events.UIEvent;
import openfl.Lib;
import openfl.net.URLRequest;

@:build(haxe.ui.toolkit.core.Macros.buildController("assets/ui/quiz.xml"))
class Quiz extends Screen {
	
	var correct:Int;
	var finishTxt:String;
	var questionNum:Int;
	var used5050:Bool;
	
	public function new(question:Int = 1) {
		questionNum = question;
		
		beginQuestion(question);
		
		quizBtn1.onClick = function (e:UIEvent) {onClick(1, quizBtn1);}
		quizBtn2.onClick = function (e:UIEvent) {onClick(2, quizBtn2);}
		quizBtn3.onClick = function (e:UIEvent) {onClick(3, quizBtn3);}
		quizBtn4.onClick = function (e:UIEvent) { onClick(4, quizBtn4); }
		
		btn5050.onClick = function (e:UIEvent) { on5050(); }
	}
	
	function beginQuestion(question:Int) {
		quizTxt.text = "Pergunta " + question + "\n";
		quizBtn1.text = "A. ";
		quizBtn2.text = "B. ";
		quizBtn3.text = "C. ";
		quizBtn4.text = "D. ";
		finishTxt = "Certo! \n";
		switch(question) {
			case 1:
				quizTxt.text += "Malware é um software...";
				quizBtn1.text += "De leitor de música";
				quizBtn2.text += "De anti-vírus";
				quizBtn3.text += "Para se infiltrar num PC";
				quizBtn4.text += "Para jogar na Internet";
				correct = 3;
				finishTxt += "O termo 'malware' é proveniente do inglês 'malicious software' ('software malicioso'); é um software destinado a infiltrar-se num sistema de computador alheio de forma ilícita, com o intuito de causar alguns danos, alterações ou roubo de informações (confidenciais ou não).";
			case 2:
				quizTxt.text += "Qual dos seguintes nomes não é um Malware?";
				quizBtn1.text += "Windows 7";
				quizBtn2.text += "Worms";
				quizBtn3.text += "Spywares";
				quizBtn4.text += "Torjan";
				correct = 1;
				finishTxt += "O termo 'malware' é proveniente do inglês 'malicious software' ('software malicioso'); é um software destinado a infiltrar-se num sistema de computador alheio de forma ilícita, com o intuito de causar alguns danos, alterações ou roubo de informações (confidenciais ou não).";
			case 3:
				quizTxt.text += "Quando se afirma, 'o João foi alvo de Phishing', significa que...";
				quizBtn1.text += "O João recebeu dinheiro";
				quizBtn2.text += "O joão recebeu um mail";
				quizBtn3.text += "O joão foi pescar";
				quizBtn4.text += "Tentaram roubar algo";
				correct = 4;
				finishTxt += "Em computação, phishing, termo oriundo do inglês (fishing) que quer dizer pesca, é uma forma de fraude eletrônica, caracterizada por tentativas de adquirir dados pessoais de diversos tipos.";
		}
	}
	
	function onClick(btnNum:Int, btn:Button) {
		if (btnNum == correct) {
			btn.style.backgroundColor = 0x00FF00;
			quizBtn1.onClick = null;
			quizBtn2.onClick = null;
			quizBtn3.onClick = null;
			quizBtn4.onClick = null;
			Timer.delay(finishQuestion, 1000);
		} else {
			btn.style.backgroundColor = 0xFF0000;
			btn.onClick = null;
		}
	}
	
	function finishQuestion() {
		var end1; // quando se carrega em continuar
		var end2; // quando se carrega em ver mais informacoes
		
		if (questionNum != 3) {
			end1 = function () {Main.switchScreen(new Quiz(questionNum + 1));}
			end2 = end1;
		} else {
			end1 = function () {Main.switchScreen(new LevelSelect());}
			end2 = function () {

				#if !flash
				Lib.getURL(new haxe.web.URLRequest("http://lmgtfy.com/?q=phishing"), "_blank");
				#end
				Main.switchScreen(new LevelSelect());
			}
		}
		
		new CommonPopup(finishTxt, "Continuar", "Pesquisar mais informações", end1, end2, "Não existem mais informações.");
	}
	
	function on5050 () {
		btn5050.style.backgroundColor = 0x404040;
		btn5050.invalidate();
		if (!used5050) {
			var ints:Array<Int> = Random.shuffle([1, 2, 3, 4]);
			var hidden = 0;
			while (hidden < 2) {
				var pick = ints.pop();
				if (pick == correct) {
					// skip, do not hide correct answer
					continue;
				} else {
					var btn = getBtn(pick);
					btn.alpha = 0;
					btn.onClick = null;
					hidden++;
				}
			}
			used5050 = true;
		}
	}
	
	override function onVoltar(_) {
		Main.switchScreen(new LevelSelect());
	}
	
	function getBtn(num:Int):Button {
		return(Reflect.getProperty(this, "quizBtn" + num));
	}
	
	
}